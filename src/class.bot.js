class Bot {
  constructor({
    name,
    id,
    avatar,
    actions
  }) {
    this.id = id;
    this.name = name;
    this.avatar = avatar;
    this.actions = actions;
  }

  findActionByKeyWord(keyWord) {
    return this.actions.filter((action) => {
      const isTrue = action.words.filter((word) => word === keyWord);

      if (isTrue.length) {
        return true;
      }

      return false;
    });
  }
}

export default Bot;
