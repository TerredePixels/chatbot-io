import TchatBot from './class.tchat-bot';

import './index.scss';

const bots = [{
  id: '1',
  name: 'Superman',
  avatar: 'https://www.ecranlarge.com/media/cache/200x200/uploads/image/000/999/superman-photo-999055.jpg',
  actions: [{
    name: 'hello world',
    words: ['bonjour', 'hello', 'toto'],
    result: () => 'Hello world'
  }, {
    name: 'clock',
    words: ['heure', 'time', 'hour', 'toto'],
    result: () => {
      const date = new Date();

      return `it's ${date.toLocaleTimeString('fr-FR')}`;
    }
  }]
}, {
  id: '2',
  name: 'DeadPool',
  avatar: 'https://64.media.tumblr.com/1f2b4c580a9a435a76e7caf795d9ac22/c4d7e7ace63a8a3c-5d/s540x810/4cf814baa36d7d6e97d35b3f511d916af40c6652.jpg',
  actions: [{
    name: 'hello world',
    words: ['bonjour', 'hello'],
    result: () => 'Hello world'
  }, {
    name: 'clock',
    words: ['heure', 'time', 'hour'],
    result: () => {
      const date = new Date();

      return `it's ${date.toLocaleTimeString('fr-FR')}`;
    }
  }]
}];

const tchatBot = new TchatBot(bots);

tchatBot.run();
