import { Modal } from 'bootstrap';
import Bot from './class.bot';
import Messages from './class.messages';

if (localStorage.getItem('messages') == null) {
  localStorage.setItem('messages', '[]');
}

class TchatBot {
  constructor(bots) {
    this.bots = bots.map((bot) => new Bot(bot));
    this.messages = JSON.parse(localStorage.getItem('messages')).map((message) => new Messages(message));
    this.elApp = document.querySelector('#app');
  }

  renderHeader() {
    return `
      <nav class="navbar navbar-dark bg-dark">
        <div class="container-fluid">
          <span class="navbar-brand mb-0 h1">Chatbot.IO</span>
        </div>
      </nav>
    `;
  }

  renderListBots() {
    return `
      <ul class="list-group list-group-flush">
        ${this.bots.map((bot) => this.renderBot(bot)).join('')}
      </ul>
    `;
  }

  renderBot(bot) {
    const { name, avatar } = bot;
    return `
      <li class="${name} bg-dark text-light list-group-item d-flex justify-content-between align-items-center">
        <img width="50" class="rounded-circle border border-white border-2" src="${avatar}" />
        ${name}
        <span class="nbr-Message badge bg-primary rounded-pill">0</span>
      </li>
    `;
  }

  renderListMessageStocked() {
    return `
  ${this.messages.map((message) => {
    const { type } = message;

    if (type === 'user') {
      return this.renderMessageStocked(message);
    }
    return this.renderMessageStockedReceived(message);
  }).join('')}
    `;
  }

  renderMessageStocked(message) {
    const {
      name,
      avatar,
      date,
      text
    } = message;

    return `
      <!-- Message 2 -->
      <div class="row mt-2">
        <div class="col-6"></div>
        <div class="col-6">
          <div class="card text-bg-light">
            <h5 class="card-header">
              <img  width="80px" height="80px" src="${avatar}" class="rounded-circle img-thumbnail" alt="...">
              ${name}
            </h5>
            <div class="card-body">
              <h5 class="card-title">${date}</h5>
              <p class="card-text">${text}</p>
            </div>
          </div>
        </div>
      </div>
    `;
  }

  renderMessageStockedReceived(message) {
    const {
      id,
      name,
      avatar,
      date,
      text
    } = message;

    return `
      <!-- Message 1 -->
      <div class="row mt-2">
        <div class="col-6">
          <div class="card text-bg-light">
            <h5 id="${id}" class="card-header">
              <img  width="80px" height="80px" src="${avatar}" class="rounded-circle img-thumbnail" alt="...">
             ${name}
            </h5>
            <div class="card-body">
              <h5 class="card-title">${date}</h5>
              <p class="card-text">${text}</p>
            </div>
          </div>
        </div>
        <div class="col-6"></div>
      </div>
    `;
  }

  renderHistoryMessages() {
    return `
      <section class="messages-history">${this.renderListMessageStocked()}}</section>
    `;
  }

  renderMessageSended(text) {
    const date = new Date();

    return `
      <!-- Message 2 -->
      <div class="row mt-2">
        <div class="col-6"></div>
        <div class="col-6">
          <div class="card text-bg-light">
            <h5 class="card-header">
              <img  width="80px" height="80px" src="https://avatarfiles.alphacoders.com/149/thumb-149117.jpg" class="rounded-circle img-thumbnail" alt="...">
              Cyril
            </h5>
            <div class="card-body">
              <h5 class="card-title">${date.toLocaleString('fr-FR')}</h5>
              <p class="card-text">${text}</p>
            </div>
          </div>
        </div>
      </div>
    `;
  }

  renderMessageReceived(id, name, avatar, text) {
    const date = new Date();

    return `
      <!-- Message 1 -->
      <div class="row mt-2">
        <div class="col-6">
          <div class="card text-bg-light">
            <h5 id="${id}" class="card-header">
              <img  width="80px" height="80px" src="${avatar}" class="rounded-circle img-thumbnail" alt="...">
             ${name}
            </h5>
            <div class="card-body">
              <h5 class="card-title">${date.toLocaleString('fr-FR')}</h5>
              <p class="card-text">${text}</p>
            </div>
          </div>
        </div>
        <div class="col-6"></div>
      </div>
    `;
  }

  renderTyping() {
    return `
      <section class="typing mt-3">
        <div class="row">
          <div class="col-12">
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Message">
                <button class="btn btn-primary" type="button" >Send</button>
              </div>
          </div>
        </div>
      </section>
    `;
  }

  renderModal() {
    return `
    <div id="myModal" class="modal">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content bg-danger text-white">
          <div class="modal-header">
            <h5 class="modal-title">Erreur</h5>
            <button type="button" class="btn-close bg-light" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <p></p>
          </div>
        </div>
      </div>
    </div>
    `;
  }

  counterMessagesBots(name, bot) {
    const nbrMessage = document.querySelector(`.${name} .nbr-Message`);
    const newNbr = Math.floor(nbrMessage.innerHTML) + Math.floor(bot.results.length);
    nbrMessage.innerHTML = newNbr;
  }

  renderMessagesBots(keyWord) {
    const responses = this.getBotsResponses(keyWord);
    const date = new Date();
    let results = [];

    responses.forEach((bot) => {
      const {
        id,
        name,
        avatar
      } = bot;

      results = results.concat(bot.results.map((result) => this.renderMessageReceived(
        id,
        name,
        avatar,
        result.result()
      )).join(''));

      bot.results.map((result) => this.addItemLocalStorage('messages', id, 'bot', name, avatar, date.toLocaleString('fr-FR'), result.result()));
    });

    return results;
  }

  addItemLocalStorage(item, id, type, name, avatar, date, text) {
    const storageMessage = JSON.parse(localStorage.getItem(item));
    storageMessage.push({
      id,
      type,
      name,
      avatar,
      date,
      text
    });
    localStorage.setItem(item, JSON.stringify(storageMessage));
  }

  eventSendMessage() {
    const elInput = document.querySelector('.typing input');

    const sendMessage = () => {
      const date = new Date();
      const elHistoryMessages = document.querySelector('.messages-history');

      if (!elInput.value) {
        this.showModal('myModal', 'Pas de message à envoyer', 2000);
        return;
      }
      this.addItemLocalStorage('messages', '11', 'user', 'Cyril', 'https://avatarfiles.alphacoders.com/149/thumb-149117.jpg', date.toLocaleString('fr-FR'), elInput.value);
      elHistoryMessages.innerHTML += this.renderMessageSended(elInput.value);
      elHistoryMessages.innerHTML += this.renderMessagesBots(elInput.value);
      elHistoryMessages.scrollTop = elHistoryMessages.scrollHeight;
      elInput.value = '';
    };

    const elBtn = document.querySelector('.typing button');

    elBtn.addEventListener('click', sendMessage);
    elInput.addEventListener('keyup', (e) => {
      if (e.keyCode === 13) sendMessage();
    });
  }

  showModal(id, text, timer) {
    const el = document.querySelector(`#${id} p`);

    el.textContent = text;

    this.modal.show();

    setTimeout(() => {
      this.modal.hide();
    }, timer);
  }

  getBotsResponses(keyWord) {
    const messages = [];

    this.bots.forEach((bot) => {
      const { id, name, avatar } = bot;
      const results = bot.findActionByKeyWord(keyWord);

      if (!results.length) {
        return;
      }

      messages.push({
        id,
        name,
        avatar,
        results
      });
    });

    return messages;
  }

  render() {
    return `
      <header>
        ${this.renderHeader()}
      </header>
      <main class="container-fluid mt-3">
        <div class="row">
          <div class="col-3">
            ${this.renderListBots()}
          </div>
          <div class="col-9">
            ${this.renderHistoryMessages()}
            ${this.renderTyping()}
          </div>
        </div>
        ${this.renderModal()}
      </main>
    `;
  }

  run() {
    this.elApp.innerHTML = this.render();
    this.eventSendMessage();
    this.modal = new Modal('#myModal', {
      keyboard: false
    });
  }
}

export default TchatBot;
