class Messages {
  constructor({
    id,
    type,
    name,
    avatar,
    date,
    text
  }) {
    this.id = id;
    this.type = type;
    this.name = name;
    this.avatar = avatar;
    this.date = date;
    this.text = text;
  }
}

export default Messages;
